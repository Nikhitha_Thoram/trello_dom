const apiKey = "6ca9c13439675ea1e194e487cd48fcfe";
const token =
  "ATTA1aaeb306f21934e53d31275f169172740a30580f2e7b094765dd8993923424623BB94747";

window.onload = function () {
  const urlParams = new URLSearchParams(window.location.search);
  const boardId = urlParams.get("boardId");
  const boardName = urlParams.get("boardName");
  console.log(boardId);
  if (boardId) {
    fetchLists(boardId, boardName);
  }
  listForm.addEventListener("submit", function (event) {
    event.preventDefault();

    const listName = listNameInput.value;

    if (!listName) {
      errorMessage.textContent = "Please enter a List name.";
      return;
    }

    errorMessage.textContent = "";

    createList(boardId, listName);
  });
};

const boardHeading=document.getElementById('boardHeading');

const listsSection = document.getElementById("lists_section");

function fetchLists(boardId, boardName) {

  boardHeading.textContent = boardName;
  return fetch(
    `https://api.trello.com/1/boards/${boardId}/lists?key=${apiKey}&token=${token}`
  )
    .then((response) => response.json())
    .then((data) => {
      displayLists(data, boardName);
    })
    .catch((error) => console.error("Error fetching lists:", error));
}

function displayEveryLists(listInfo) {
  const listElement = document.createElement("div");
  listElement.className = "card";

  const listContentContainer = document.createElement("div");
  listContentContainer.className =
    "list-header flex flex-row justify-between gap-8 p-2 items-center";

  const listNameElement = document.createElement("div");
  listNameElement.textContent = listInfo.name;
  listElement.id = listInfo.id;
  const deleteButton = document.createElement("button");
  deleteButton.className = "delete-button";
  deleteButton.innerHTML = `x`;
  deleteButton.addEventListener("click", function () {
    deleteList(listInfo.id)
      .then(() => {
        listElement.remove();
      })
      .catch((error) => {
        console.error("Error deleting list:", error);
      });
  });

  listContentContainer.appendChild(listNameElement);
  listContentContainer.appendChild(deleteButton);

  listElement.appendChild(listContentContainer);
  let cardContainer = document.createElement("div");
  cardContainer.id = "card-container";
  cardContainer.classList.add("mt-2");
  listElement.appendChild(cardContainer);
  getCards(listInfo);

  const cardDiv = document.createElement("div");
  cardDiv.className = "card-div";

  const cardButton = document.createElement("button");
  cardButton.innerHTML = `+ Add a card`;

  cardDiv.appendChild(cardButton);
  listElement.appendChild(cardDiv);

  cardButton.addEventListener("click", function () {
    openModal(listInfo);
  });

  listsSection.appendChild(listElement);
}

function displayLists(listData, boardName) {
  listData.forEach((list) => {
    displayEveryLists(list);
  });
}

const modalList = document.getElementById("modalList");
const listForm = document.getElementById("listForm");
const errorMessage = document.getElementById("errorMessage");
const cancelBtn = document.getElementById("cancelBtn");
const listNameInput = document.getElementById("listName");

function showListModel() {
  modalList.classList.remove("hidden");
}

function hideListModel() {
  modalList.classList.add("hidden");
}

const createListBtn = document.getElementById("createListBtn");
createListBtn.addEventListener("click", showListModel);

cancelBtn.addEventListener("click", hideListModel);

function createList(boardId, listName) {
  return fetch(
    `https://api.trello.com/1/lists/?name=${listName}&idBoard=${boardId}&key=${apiKey}&token=${token}`,
    {
      method: "POST",
    }
  )
    .then((response) => {
      return response.json();
    })
    .then((list) => {
      displaySpecificList(list);
      hideListModel();
      cardNameInput.value = "";
    })

    .catch((error) => {
      console.error("Error fetching data", error);
    });
}

function displaySpecificList(listData) {
  displayEveryLists(listData);
}

function deleteList(listId) {
  return fetch(
    `https://api.trello.com/1/lists/${listId}/closed?key=${apiKey}&token=${token}&value=true`,
    {
      method: "PUT",
    }
  )
    .then((response) => {
      return response.json();
    })
    .catch((error) => {
      console.error(error);
    });
}

function getCards(listData) {
  let listId = listData.id;
  return fetch(
    `https://api.trello.com/1/lists/${listId}/cards?key=${apiKey}&token=${token}`
  )
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      displayCards(data);
    })

    .catch((error) => {
      console.error("Error fetching data", error);
    });
}

function displayCards(cardDataEachList) {
  cardDataEachList.forEach((card) => {
    displayEachCard(card);
  });
}

function displayEachCard(cardData) {
  let listDataCards = document.getElementById(cardData.idList);
  console.log(listDataCards);
  let cardContainer = listDataCards.querySelector("#card-container");
  let newCard = document.createElement("div");
  newCard.id = cardData.id;
  newCard.textContent = cardData.name;
  newCard.classList.add("text-black","rounded-md","relative","p-2","border","font-normal","mt-2","my-4","mr-4","bg-white","flex","justify-between");
  newCard.style.marginBottom = "10px";
  let deleteCardIcon = document.createElement("button");
  deleteCardIcon.innerHTML = `x`;
  deleteCardIcon.addEventListener("click", () => {
    deleteCard(cardData);
  });
  cardContainer.appendChild(newCard);
  newCard.appendChild(deleteCardIcon);
}

const cardNameInput = document.getElementById("cardName");

function openModal(listData) {
    const modal = document.getElementById('cardModal');
    const newCardForm = document.getElementById('cardForm');
    const closeModalButton = document.getElementById('cardCancelBtn'); 

    newCardForm.onsubmit = null;
    closeModalButton.onclick = null;
  
  
    newCardForm.onsubmit = (event) => {
      event.preventDefault();
      const cardName = cardNameInput.value;
      if (cardName !== '') {
        createCards(listData, cardName);
        modal.classList.add('hidden');
      }
    };
  
    closeModalButton.onclick = (event) => {
      event.preventDefault();
      modal.classList.add('hidden');
    };
  
    modal.classList.remove('hidden');
  }
  
function createCards(listData, cardName) {
  let listId = listData.id;
  return fetch(
    `https://api.trello.com/1/cards/?name=${cardName}&idList=${listId}&key=${apiKey}&token=${token}`,
    {
      method: "POST",
    }
  )
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      displayEachCard(data);
      hideCardModel();
      cardNameInput.value = "";
    })
    .catch((error) => {
      console.error("Error fetching data", error);
    });
}

function deleteCard(cardData) {
  let id = cardData.id;
  fetch(`https://api.trello.com/1/cards/${id}?key=${apiKey}&token=${token}`, {
    method: "DELETE",
  })
    .then((response) => {
      return response.json();
    })
    .then(() => {
      closeCard(cardData);
    })

    .catch((err) => console.error(err));
}
function closeCard(card) {
  let deleteCard = document.getElementById(card.id);
  deleteCard.remove();
}
