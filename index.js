const apiKey = "6ca9c13439675ea1e194e487cd48fcfe";
const token =
  "ATTA1aaeb306f21934e53d31275f169172740a30580f2e7b094765dd8993923424623BB94747";

function fetchBoards() {
  return (
    fetch(
      `https://api.trello.com/1/members/me/boards?key=${apiKey}&token=${token}`
    )
    .then((response) => response.json())
      // .then((data)=>{
      //   console.log(data);
      // })
    .catch((error) => console.error("Error fetching boards:", error))
  );
}   

const boardsSection = document.getElementById("boards_section");
function displayBoards(boards) {
  boards.forEach((board) => {
    const boardElement = document.createElement("button");
    boardElement.textContent = board.name;
    boardElement.dataset.boardId = board.id;
    boardElement.className = "board";
    boardsSection.appendChild(boardElement);
    console.log(boardElement);
  });
}

function fetchBoardsAndDisplay() {
  fetchBoards()
    .then((boards) => {
      displayBoards(boards);
    })
    .catch((error) => {
      console.error("Error fetching and displaying boards:", error);
    });
}

fetchBoardsAndDisplay();

const modalBoard = document.getElementById("modalBoard");
const createBoardButton = document.getElementById("createBoard_button");
const boardForm = document.getElementById("boardForm");
const boardNameInput = document.getElementById("boardName");
const submit = document.getElementById("submit");
const cancelButton = document.getElementById("cancelBtn");

function showModelBoard() {
  modalBoard.classList.remove("hidden");
}

function hideModelBoard() {
  modalBoard.classList.add("hidden");
}

createBoardButton.addEventListener("click", showModelBoard);
cancelButton.addEventListener("click", hideModelBoard);

boardForm.addEventListener("submit", function (event) {
  event.preventDefault();

  const boardName = boardNameInput.value;
  const errorMessageContainer = document.getElementById("errorMessage");
  if (!boardName) {
    errorMessageContainer.textContent = "Please enter a board name.";
    return;
  }

  errorMessageContainer.textContent = "";

  createBoard(boardName);
});

function createBoard(boardName) {
  return fetch(
    `https://api.trello.com/1/boards/?name=${boardName}&key=${apiKey}&token=${token}`,
    {
      method: "POST",
    }
  )
    .then((response) => {
      return response.json();
    })
    .then((board) => {
      displayNewBoard(board);
      hideModelBoard();
      boardNameInput.value = "";
    })

    .catch((error) => {
      console.error("Error fetching data", error);
    });
}

function displayNewBoard(board) {
  const boardElement = document.createElement("button");
  boardElement.textContent = board.name;
  boardElement.dataset.boardId = board.id;
  boardElement.className = "board";
  boardsSection.appendChild(boardElement);
}

boardsSection.addEventListener("click", function (event) {
  const clickedBoard = event.target;
  if (clickedBoard.classList.contains("board")) {
    const boardId = clickedBoard.dataset.boardId;
    const boardName = clickedBoard.textContent.trim();
    if (boardId) {
      const url = `boardData.html?boardId=${boardId}&boardName=${boardName}`;
      window.location.href = url;
    } else {
      console.error("Board ID not found:", boardId);
    }
  }

});
