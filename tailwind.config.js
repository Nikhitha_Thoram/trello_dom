/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [
      './index.html',
      './boardData.html'
    ],
    theme: {
      extend: {
        fontFamily: {
            'charlie-sans': ['Charlie Sans', 'sans-serif']
          }
      },
    },
    plugins: [],
}
  
  